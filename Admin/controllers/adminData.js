// controllers/dataController.js
const express = require('express');
const Data = require('../models/data'); // Import your Data model

const router = express.Router();

// Retrieve all data
router.get('/data', async (req, res) => {
  try {
    const data = await Data.find();
    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred' });
  }
});

// Create a new data entry
router.post('/data', async (req, res) => {
  try {
    const newData = req.body;
    const data = await Data.create(newData);
    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred' });
  }
});

// Update an existing data entry
router.put('/data/:id', async (req, res) => {
  try {
    const dataId = req.params.id;
    const newData = req.body;
    const data = await Data.findByIdAndUpdate(dataId, newData, { new: true });
    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred' });
  }
});

// Delete a data entry
router.delete('/data/:id', async (req, res) => {
  try {
    const dataId = req.params.id;
    const data = await Data.findByIdAndRemove(dataId);
    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred' });
  }
});

module.exports = router;
