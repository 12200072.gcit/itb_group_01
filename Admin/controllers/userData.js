const Data = require('../models/userPostModel');
const path = require('path'); // Add this require for working with file paths
const multer = require('multer');
const Admin=require('../models/dataModel')







const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadDir = 'public/uploads/';

    // Create the directory if it doesn't exist
    if (!fs.existsSync(uploadDir)) {
      fs.mkdirSync(uploadDir, { recursive: true });
    }

    cb(null, uploadDir);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });


exports.postUser = async (req, res) => {
  upload.single('image')(req, res, async (err) => {
    if (err) {
      return res.status(400).json({ error: 'Error uploading the image' });
    }

    const { date, name, currentlocation, source, destination, description } = req.body;

    try {
      // Format the date as "yyyy-MM-dd" (e.g., "2023-10-05")
      const formattedDate = new Admin(date).toISOString().split('T')[0];

      // Create a new data entry
      const newData = new Admin({
        date: formattedDate,
        name,
        currentlocation,
        source,
        destination,
        description,
        image: `http://localhost:3002/uploads/${req.file.filename}`, // URL to the image
      });

      await newData.save();
      res.redirect('/loading1.html');
    } catch (err) {
      res.status(500).json({ error: 'Server error' });
    }
  });
};



exports.userupdateDataById = async (req, res) => {
  const dataId = req.params.id;
  const updatedData = req.body;

  try {
      // Find the data by ID
      const data = await Data.findById(dataId);

      if (!data) {
          return res.status(404).json({ error: 'Data entry not found' });
      }

      // Update the fields you want to change
      data.date = updatedData.date;
      data.name = updatedData.name;
      data.currentlocation = updatedData.currentlocation;
      data.source = updatedData.source;
      data.destination = updatedData.destination;
      data.description =updatedData.description
   

      if (req.file) {
        // Remove the old image file from your server (if needed)
        // Update the reference to the image URL with the new file name
        data.image = `http://localhost:3002/uploads/${req.file.filename}`;
      }

      // Save the updated data
      await data.save();
     
  } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'An error occurred while updating data' });
  }
};


// Get all data entries
exports.getAllUser = async (req, res) => {
  try {
    const data = await Data.find();
    res.json(data);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching data' });
  }
};

// Delete a data entry by ID
exports.deleteUserData = async (req, res) => {
  const dataId = req.params.id; // Get the data entry ID from the request parameters

  try {
    // Find the data entry by ID and remove it
    const data = await Data.findByIdAndRemove(dataId);

    if (!data) {
      return res.status(404).json({ error: 'Data entry not found' });
    }

    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while deleting data' });
  }
};



