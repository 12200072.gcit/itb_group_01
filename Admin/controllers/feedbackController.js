const Data = require('../models/feedbackModel');
const path = require('path'); // Add this require for working with file paths

exports.postFeedback = async (req, res) => {
  const { name,description } = req.body;

  try {
    // Create a new data entry with only name and description fields
    const newData = new Data({
      name,
      description,
    });

    await newData.save();
   
  } catch (err) {
    res.status(500).json({ error: 'Server error' });
  }
  };
  
  exports.getAllFeedback = async (req, res) => {
    try {
      const data = await Data.find();
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while fetching data' });
    }
  };

  exports.deleteFeedback = async (req, res) => {
    const dataId = req.params.id; // Get the data entry ID from the request parameters
  
    try {
      // Find the data entry by ID and remove it
      const data = await Data.findByIdAndRemove(dataId);
  
      if (!data) {
        return res.status(404).json({ error: 'Data entry not found' });
      }
  
      res.json(data);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'An error occurred while deleting data' });
    }
  };
  