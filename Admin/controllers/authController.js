const User = require('../models/userModels');
const path = require('path'); // Add this require for working with file paths
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


exports.registerUser = async (req, res) => {
  const { name,email, password, phone } = req.body;

  try {
    // Check if the email is already registered
    const existingUser = await User.findOne({ email });

    if (existingUser) {
      return res.status(400).send("Email already used.");
    }

    // Hash the password before saving it
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    // Create a new user with the hashed password
    const user = new User({
      name,
      email,
      password: hashedPassword,
      phone,
    });

    await user.save();

    // Generate a JWT token
    const token = jwt.sign({ email: user.email }, 'pemanorbu', { expiresIn: '1h' });

    // Include the token in the response
    res.json({ Success: 'User registered', token });
  } catch (err) {
    console.error(err);
    return res.status(500).send("Internal Server Error");
  }
};


exports.loginUser = async (req, res) => {
  const { email, password } = req.body;

  try {
    // Find a user by email
    const user = await User.findOne({ email });

    if (!user) {
      // User not found
      const errorMessage = "Email not registered.";
      return res.redirect(`/login.html?error=${encodeURIComponent(errorMessage)}`);
    }

    // Compare the hashed password from the database with the provided password
    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      const errorMessage = "Incorrect password";
      return res.redirect(`/login.html?error=${encodeURIComponent(errorMessage)}`);
    }

    // Authentication successful; generate a new token
    const token = jwt.sign({ email: user.email }, 'pema', { expiresIn: '1h' });

    // Include the token in the response
    res.redirect(`/loading.html?token=${token}`);
  } catch (error) {
    console.error(error);
    return res.status(500).send("Error: Login failed");
  }
};


exports.homepage = async (req, res) => {
  try {
    // Fetch data from MongoDB (e.g., all users)
    const users = await User.find({});

    // Send the data as JSON
    res.json({ users });
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
};



