
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const User = require('../models/userModels');
const bcrypt = require('bcrypt');

exports.sendResetEmail = async (req, res) => {
  const { email } = req.body;

  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
    console.log(user)

    const resetToken = crypto.randomBytes(20).toString('hex');
    const resetTokenExpiration = Date.now() + 3600000; // 1 hour in milliseconds

    user.resetPasswordToken = resetToken;
    user.resetPasswordExpires = resetTokenExpiration;
    await user.save();

    const transporter = nodemailer.createTransport({
        host: 'smtp.example.com', // Replace with your SMTP server hostname
        port: 587, // Replace with your SMTP server port
        secure: false, // Set to true for TLS, false for non-secure connection
        service: 'Gmail', // Use the Gmail service

        auth: {
          user: '12200072.gcit@rub.edu.bt', // Your Gmail email address
          pass: 'zjsmnkjikaxjvgmv', // Your Gmail email password
        },
    });

    const mailOptions = {
      from: '12200072.gcit@rub.edu.bt',
      to: user.email,
      subject: 'Password Reset',
    //   text: `You are receiving this email because you have requested a password reset. 
    //     Please click the following link to reset your password: <http://${req.headers.host}/reset-password/${resetToken}>
    //     If you didn't request this, please ignore this email.`,
    // };
    html: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
    '<a href="http://' + req.headers.host + '/resetadminPassword/' + resetToken + '">Reset Your Pasword</a>\n\n' +
    'If you did not request this, please ignore this email and your password will remain unchanged.\n',}

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error(error);
        return res.status(500).json({ message: 'Error sending email' });
      } else {
        console.log('Email sent: ' + info.response);
        return res.status(200).json({ message: 'Password reset email sent' });
       
       
      }
    });
    
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};



exports.renderResetForm = (req, res) => {
    // Render the password reset form
    res.render('password-reset', { token: req.params.token });
  };
  




  exports.resetPassword1 = async (req, res) => {
    const { token, password } = req.body;
  
    try {
      const user = await User.findOne({
        resetPasswordToken: token,
        resetPasswordExpires: { $gt: Date.now() },
      });
  
      if (!user) {
        return res.status(400).json({ message: 'Invalid or expired token' });
      }
  
      // Update the user's password
      user.password = password;
      user.resetPasswordToken = undefined;
      user.resetPasswordExpires = undefined;
      await user.save();
  
      // You can redirect the user to a login page or any other appropriate page
      return res.status(200).json({ message: 'Password reset successful' });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Internal Server Error' });
    }
  };



