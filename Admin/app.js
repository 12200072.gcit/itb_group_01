

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use('/public', express.static('public'));



mongoose.connect('mongodb+srv://pemanorbu:pema12200072@roadstatus.d8izeg2.mongodb.net/pemanorbu?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  tlsAllowInvalidCertificates: true, // Add this option
  ssl: true,
  tls: true,

});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function () {
  console.log('Connected to MongoDB');
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Serve static files from the "views" folder
app.use(express.static(path.join(__dirname, 'views')));

// Define a route for the root URL ("/") to serve login.html
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'login.html'));
});

app.get('/viewPost', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'viewPost.html'));
});
app.get('/homepage', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'home.html'));
});
app.get('/UserPost', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'user.html'));
});
app.get('/viewfeedback', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'Feedback.html'));
});
app.get('/loading2', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'loading2.html'));
});

app.get('/resetadminPassword', (req, res) => {
  res.sendFile(path.join(__dirname, 'views', 'resetPassword.html'));
});






const passwordResetRoutes = require('./routes/passwordResetRoutes');
app.use('/password-reset', passwordResetRoutes);


const dataFeedback=require('./routes/feedbackRoute')

app.use('/feed', dataFeedback);

const userdataRoute = require('./routes/userDataRoute');
app.use('/userpost',userdataRoute);

const authRoutes = require('./routes/userRoutes');

const dataRoutes = require('./routes/dataRoutes');
app.use('/api', dataRoutes);

app.use('/login.html', authRoutes);
app.use('/auth', authRoutes);


module.exports = app;


