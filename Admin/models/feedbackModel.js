const mongoose = require('mongoose');
const { Schema } = mongoose;

const feedbackSchema = new mongoose.Schema({
  name: String,
  description: String,
});

const Data = mongoose.model('UserFeedback',feedbackSchema);

module.exports = Data;
