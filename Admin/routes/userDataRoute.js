const express = require('express');
const router = express.Router();

const datauserController = require('../controllers/userData');

const multer = require('multer');
const fs = require('fs');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadDir = 'public/uploads/';

    // Create the directory if it doesn't exist
    if (!fs.existsSync(uploadDir)) {
      fs.mkdirSync(uploadDir, { recursive: true });
    }

    cb(null, uploadDir);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });



router.get('/postuser',datauserController.postUser);

router.get('/viewuserPost/:id',datauserController.getAllUser);
router.put('/user-update-data/:id',upload.single('image'),datauserController.userupdateDataById);
router.delete('/userdelete/:id', datauserController.deleteUserData);




module.exports = router;
