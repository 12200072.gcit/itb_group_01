const express = require('express');
const router = express.Router();

const dataController = require('../controllers/dataController');
const multer = require('multer');
const fs = require('fs');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadDir = 'public/uploads/';

    // Create the directory if it doesn't exist
    if (!fs.existsSync(uploadDir)) {
      fs.mkdirSync(uploadDir, { recursive: true });
    }

    cb(null, uploadDir);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });


router.post('/post-data', dataController.postData);
router.post('/postuserdata', dataController.postUserData);


router.get('/viewPost/:id',dataController.getAllData);
router.put('/update-data/:id',upload.single('image'),dataController.updateDataById);
router.delete('/data/:id', dataController.deleteData);

module.exports = router;
