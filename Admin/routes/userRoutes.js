


const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');
// const dataController = require('../controllers/dataController');
// const multer = require('multer')
// const fs = require('fs');


// Registration route
router.post('/sign_up', authController.registerUser);

// Login route
router.post('/login', authController.loginUser);

router.get('/homepage', authController.homepage);



// Logout
router.get('/logout', (req, res) => {
    console.log("logout");
    if (req.session) {
      // Destroy the session
      req.session.destroy(function (err) {
        if (err) {
          console.error(err);
          return res.status(500).send("Error: Logout failed");
        } else {
          // Redirect to the home page or login page after logout
          res.redirect('/'); // Redirect to the root URL (home page)
        }
      });
    } else {
      // If there's no session, simply redirect to the home page
      res.redirect('/'); // Redirect to the root URL (home page)
    }
  });
module.exports = router;
