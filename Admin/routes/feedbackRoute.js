const express = require('express');
const router = express.Router();

const feedbackController=require('../controllers/feedbackController')


router.post('/post-feedback',feedbackController.postFeedback)
router.get('/get-feed',feedbackController.getAllFeedback)
router.delete('/deleteFeedback/:id', feedbackController.deleteFeedback);



module.exports = router;
