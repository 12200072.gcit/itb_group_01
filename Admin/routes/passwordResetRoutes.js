const express = require('express');
const router = express.Router();
const passwordResetController = require('../controllers/passwordResetController');

// // Send a password reset email
// router.post('/sendResetEmail', passwordResetController.sendResetEmail);

// router.get(`/reset/:token`, passwordResetController.showResetForm);

// // Handle the password reset form submission
// router.post('/reset', passwordResetController.resetPassword);

router.post('/send-reset-email', passwordResetController.sendResetEmail);
router.get('/reset-password/:token', passwordResetController.renderResetForm);

// Route to handle the password reset form submission
router.post('/reset-password1', passwordResetController.resetPassword1);

module.exports = router;
