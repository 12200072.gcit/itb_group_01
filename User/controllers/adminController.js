const Data = require('../model/adminModel');
const path = require('path'); // Add this require for working with file paths
const multer = require('multer');
const fs = require('fs');



const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadDir = 'public/uploads/';

    // Create the directory if it doesn't exist
    if (!fs.existsSync(uploadDir)) {
      fs.mkdirSync(uploadDir, { recursive: true });
    }

    cb(null, uploadDir);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });




// Get all data entries
exports.getAllData = async (req, res) => {
  try {
    const data = await Data.find();
    res.json(data);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching data' });
  }
};




// Delete a data entry by ID
exports.deleteData = async (req, res) => {
  const dataId = req.params.id; // Get the data entry ID from the request parameters

  try {
    // Find the data entry by ID and remove it
    const data = await Data.findByIdAndRemove(dataId);

    if (!data) {
      return res.status(404).json({ error: 'Data entry not found' });
    }

    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while deleting data' });
  }
};

// exports.searchData = async (req, res) => {
//   try {
//     const { source, destination, date } = req.query;

//     if (!source || !destination || !date) {
//       res.status(400).json({ message: 'Source, destination, and date are required.' });
//       return;
//     }

//     // Create regular expressions for partial and case-insensitive matching
//     const sourceRegex = new RegExp(source, 'i');
//     const destinationRegex = new RegExp(destination, 'i');

//     const searchData = await Data.find({
//       $and: [
//         { source: sourceRegex },
//         { destination: destinationRegex },
//         { date: new Date(date) }  // Ensure date is an exact match (consider date format)
//       ]
//     });

//     if (searchData.length === 0) {
//       res.status(404).json({ message: 'No matching data found.' });
//     } else {
//       res.json(searchData);
//     }
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: 'An error occurred while searching data.' });
//   }
// };

exports.searchData = async (req, res) => {
  try {
    const { source, destination, date } = req.query;

    if (!source || !destination || !date) {
      res.status(400).json({ message: 'Source, destination, and date are required.' });
      return;
    }

    // Create regular expressions for partial and case-insensitive matching
    const sourceRegex = new RegExp(source, 'i');
    const destinationRegex = new RegExp(destination, 'i');

    const searchData = await Data.find({
      $and: [
        { source: sourceRegex },
        { destination: destinationRegex },
        { date: new Date(date) }  // Ensure date is an exact match (consider date format)
      ]
    });

    if (searchData.length === 0) {
      res.status(404).json({ message: 'No matching data found.' });
    } else {
      res.json(searchData);
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while searching data.' });
  }
};



