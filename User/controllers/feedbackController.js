const Data = require('../model/feedbackModel');
const path = require('path'); // Add this require for working with file paths

exports.postFeedback = async (req, res) => {
  const { name,description } = req.body;

  try {
    // Create a new data entry with only name and description fields
    const newData = new Data({
      name,
      description,
    });

    await newData.save();
    res.redirect('/loading1.html');
   
  } catch (err) {
    res.status(500).json({ error: 'Server error' });
  }
  };
  