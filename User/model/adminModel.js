const mongoose = require('mongoose');
const { Schema } = mongoose;

const dataSchema = new Schema({

  date: {
    type: Date,
    required: true,
  },
  name: {
    type: String,
    required: true,
    },
currentlocation: {
  type: String,
  required: true,
  },
  source: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true,
  },
  description: {
    type: String, // You can adjust the type based on your requirements (String, Text, etc.)
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
});

const Data = mongoose.model('Data', dataSchema);

module.exports = Data;
