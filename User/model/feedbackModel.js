const mongoose = require('mongoose');
const { Schema } = mongoose;

const feedbackSchema = new mongoose.Schema({

  name: {
    type: String,
    required: true,
    },


  description: {
    type: String, // You can adjust the type based on your requirements (String, Text, etc.)
    required: true,
  },
 
});

const Data = mongoose.model('UserFeedback',feedbackSchema);

module.exports = Data;
