const express = require('express');
const router = express.Router();

const feedbackController=require('../controllers/feedbackController')


router.post('/post-feedback',feedbackController.postFeedback)


module.exports = router;
